package com.geneticalgorithm.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.geneticalgorithm.GeneticAlgorithm;
import com.geneticalgorithm.implementations.crossover.OrderCrossover;
import com.geneticalgorithm.implementations.evaluation.NoCollisionEvaluation;
import com.geneticalgorithm.implementations.initialize.RandomInitialize;
import com.geneticalgorithm.implementations.mutation.ExchangeMutation;
import com.geneticalgorithm.implementations.selection.TournamentSelection;
import com.geneticalgorithm.interfaces.Crossover;
import com.geneticalgorithm.interfaces.Evaluation;
import com.geneticalgorithm.interfaces.InitializePopulation;
import com.geneticalgorithm.interfaces.Mutation;
import com.geneticalgorithm.interfaces.Selection;

@Configuration
@PropertySource("classpath:config-test.properties")
public class GeneticAlgorithmTestConfiguration {

    @Value("${algorithm.population.size}")
    private int populationSize;

    @Value("${algorithm.iterations.count}")
    private int iterationsCount;

    @Value("${algorithm.probability.crossover}")
    private float crossoverProbability;

    @Value("${algorithm.probability.mutation}")
    private float mutationProbability;

    @Bean
    public GeneticAlgorithm geneticAlgorithm(InitializePopulation initializePopulation, Evaluation evaluation,
            Selection selection, Crossover crossover, Mutation mutation) {
        return new GeneticAlgorithm(iterationsCount, initializePopulation, evaluation, selection, crossover, mutation);
    }

    @Bean
    public InitializePopulation initializePopulation() {
        return new RandomInitialize(populationSize);
    }

    @Bean
    public Evaluation evaluation() {
        return new NoCollisionEvaluation();
    }

    @Bean
    public Selection selection() {
        return new TournamentSelection(populationSize);
    }

    @Bean
    public Crossover crossover() {
        return new OrderCrossover(crossoverProbability);
    }

    @Bean
    public Mutation mutation() {
        return new ExchangeMutation(mutationProbability);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
