package com.geneticalgorithm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.geneticalgorithm.configuration.GeneticAlgorithmTestConfiguration;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { GeneticAlgorithmTestConfiguration.class })
public class GeneticAlgorithmTest {

    @Autowired
    private GeneticAlgorithm geneticAlgorithm;

    @Test
    public void testGeneticAlgorithm() throws Exception {
        // Its impossible to assure a solution, so the only thing to test here is that the algorithm doesn't throw exceptions
        geneticAlgorithm.run(4);
    }

}
