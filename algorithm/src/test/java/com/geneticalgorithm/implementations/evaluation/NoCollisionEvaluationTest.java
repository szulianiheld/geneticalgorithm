package com.geneticalgorithm.implementations.evaluation;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.geneticalgorithm.interfaces.Evaluation;
import com.geneticalgorithm.model.AlgorithmResult;

public class NoCollisionEvaluationTest {

    @Test
    public void testNoCollisionEvaluation() throws Exception {
        List<AlgorithmResult> population = createPopulation();
        Evaluation evaluation = new NoCollisionEvaluation();
        AlgorithmResult result = evaluation.eval(population);
        assertEquals(0, result.collisions());
    }

    private List<AlgorithmResult> createPopulation() {
        List<AlgorithmResult> parents = new ArrayList<>();
        int size = 4;
        AlgorithmResult parent1 = new AlgorithmResult(size);
        parent1.setElements(asList(0, 1, 2, 3));
        parents.add(parent1);
        AlgorithmResult parent2 = new AlgorithmResult(size);
        parent2.setElements(asList(1, 2, 3, 0));
        parents.add(parent2);
        AlgorithmResult parent3 = new AlgorithmResult(size);
        parent3.setElements(asList(2, 0, 3, 1));
        parents.add(parent3);
        AlgorithmResult parent4 = new AlgorithmResult(size);
        parent4.setElements(asList(3, 0, 1, 2));
        parents.add(parent4);
        return parents;
    }

}
