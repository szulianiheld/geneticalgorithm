package com.geneticalgorithm.implementations.crossover;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.geneticalgorithm.interfaces.Crossover;
import com.geneticalgorithm.model.AlgorithmResult;

public class OrderCrossoverTest {

    @Test
    public void testOrderCrossover() throws Exception {
        List<AlgorithmResult> parents = createParents();
        Crossover crossover = new OrderCrossover(1.0f);
        List<AlgorithmResult> children = crossover.cross(parents);
        assertEquals(4, children.size());
    }

    private List<AlgorithmResult> createParents() {
        List<AlgorithmResult> parents = new ArrayList<>();
        int size = 10;
        AlgorithmResult parent1 = new AlgorithmResult(size);
        parent1.setElements(asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        parents.add(parent1);
        AlgorithmResult parent2 = new AlgorithmResult(size);
        parent2.setElements(asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0));
        parents.add(parent2);
        AlgorithmResult parent3 = new AlgorithmResult(size);
        parent3.setElements(asList(2, 3, 4, 5, 6, 7, 8, 9, 0, 1));
        parents.add(parent3);
        AlgorithmResult parent4 = new AlgorithmResult(size);
        parent4.setElements(asList(3, 4, 5, 6, 7, 8, 9, 0, 1, 2));
        parents.add(parent4);
        return parents;
    }

}
