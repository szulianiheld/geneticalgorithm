package com.geneticalgorithm.implementations.mutation;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.geneticalgorithm.interfaces.Mutation;
import com.geneticalgorithm.model.AlgorithmResult;

public class ExchangeMutationTest {

    @Test
    public void testExchangeMutation() throws Exception {
        List<AlgorithmResult> children = createChildren();
        Mutation mutation = new ExchangeMutation(1.0f);
        List<AlgorithmResult> mutatedChildren = mutation.mutate(children);
        assertEquals(1, mutatedChildren.size());
    }

    private List<AlgorithmResult> createChildren() {
        List<AlgorithmResult> parents = new ArrayList<>();
        AlgorithmResult parent1 = new AlgorithmResult(10);
        parent1.setElements(asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9));
        parents.add(parent1);
        return parents;
    }

}
