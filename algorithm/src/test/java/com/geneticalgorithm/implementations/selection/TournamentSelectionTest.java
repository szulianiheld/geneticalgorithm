package com.geneticalgorithm.implementations.selection;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.geneticalgorithm.interfaces.Selection;
import com.geneticalgorithm.model.AlgorithmResult;

public class TournamentSelectionTest {

    @Test
    public void testTournamentSelection() throws Exception {
        int populationSize = 2;
        List<AlgorithmResult> newPopulation = createNewPopulation();
        Selection selection = new TournamentSelection(populationSize);
        List<AlgorithmResult> newGeneration = selection.select(newPopulation);
        assertEquals(2, newGeneration.size());
        assertEquals(asList(2, 0, 3, 1), newGeneration.get(0).getElements());
        assertEquals(asList(1, 2, 0, 3), newGeneration.get(1).getElements());
    }

    private List<AlgorithmResult> createNewPopulation() {
        List<AlgorithmResult> parents = new ArrayList<>();
        int size = 4;
        AlgorithmResult parent1 = new AlgorithmResult(size);
        parent1.setElements(asList(0, 1, 2, 3));
        parents.add(parent1);
        AlgorithmResult parent2 = new AlgorithmResult(size);
        parent2.setElements(asList(1, 2, 0, 3));
        parents.add(parent2);
        AlgorithmResult parent3 = new AlgorithmResult(size);
        parent3.setElements(asList(2, 0, 3, 1));
        parents.add(parent3);
        AlgorithmResult parent4 = new AlgorithmResult(size);
        parent4.setElements(asList(3, 0, 1, 2));
        parents.add(parent4);
        AlgorithmResult parent5 = new AlgorithmResult(size);
        parent5.setElements(asList(3, 1, 2, 0));
        parents.add(parent5);
        return parents;
    }

}
