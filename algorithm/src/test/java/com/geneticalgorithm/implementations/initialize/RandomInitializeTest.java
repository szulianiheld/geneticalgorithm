package com.geneticalgorithm.implementations.initialize;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.geneticalgorithm.interfaces.InitializePopulation;
import com.geneticalgorithm.model.AlgorithmResult;

public class RandomInitializeTest {

    @Test
    public void testRandomInitialize() throws Exception {
        int populationSize = 5;
        int elementSize = 4;
        InitializePopulation initialize = new RandomInitialize(populationSize);
        List<AlgorithmResult> initialPopulation = initialize.init(elementSize);
        assertEquals(5, initialPopulation.size());
        assertEquals(4, initialPopulation.get(0).getSize());
    }

}
