package com.geneticalgorithm.model;

public class AlgorithmResultWrapper {

    private AlgorithmResult algorithmResult;
    private int iterations;

    public AlgorithmResultWrapper(AlgorithmResult algorithmResult, int iterations) {
        this.algorithmResult = algorithmResult;
        this.iterations = iterations;
    }

    public int getIterations() {
        return iterations;
    }

    public AlgorithmResult getAlgorithmResult() {
        return algorithmResult;
    }

    @Override
    public String toString() {
        return "Solution found in " + iterations + " iterations. \n\n" + algorithmResult.toString();
    }

}
