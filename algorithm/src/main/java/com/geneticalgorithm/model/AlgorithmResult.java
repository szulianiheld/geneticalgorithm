package com.geneticalgorithm.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class AlgorithmResult implements Serializable {

    private List<Integer> elements;

    public AlgorithmResult(Integer size) {
        this.elements = new ArrayList<>(size);
    }

    public Integer getValue(Integer index) {
        return elements.get(index);
    }

    public void setElements(List<Integer> elements) {
        this.elements = elements;
    }

    public List<Integer> getElements() {return elements;}

    public int getSize() {
        return elements.size();
    }

    public int collisions() {
        int count = 0;
        for (int i = 0; i < elements.size() - 1; i++) {
            for (int j = i + 1; j < elements.size(); j++) {
                if (abs(elements.get(i) - elements.get(j)) == abs(i - j)) {
                    count += 1;
                }
            }
        }
        return count;
    }

    public boolean isEmpty() {
        return elements.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (int i = 0; i < elements.size(); i++) {
            out.append("|");
            for (Integer element : elements) {
                if (element == i) {
                    out.append(" X |");
                } else {
                    out.append(" - |");
                }
            }
            out.append("\n");
        }
        return out.toString();
    }
}
