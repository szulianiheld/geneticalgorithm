package com.geneticalgorithm.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.geneticalgorithm.GeneticAlgorithm;
import com.geneticalgorithm.implementations.crossover.OrderCrossover;
import com.geneticalgorithm.implementations.evaluation.NoCollisionEvaluation;
import com.geneticalgorithm.implementations.initialize.RandomInitialize;
import com.geneticalgorithm.implementations.mutation.ExchangeMutation;
import com.geneticalgorithm.implementations.selection.TournamentSelection;
import com.geneticalgorithm.interfaces.Crossover;
import com.geneticalgorithm.interfaces.Evaluation;
import com.geneticalgorithm.interfaces.InitializePopulation;
import com.geneticalgorithm.interfaces.Mutation;
import com.geneticalgorithm.interfaces.Selection;

import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@PropertySource("classpath:config.properties")
public class GeneticAlgorithmConfiguration {

    @Value("${algorithm.population.size:10}")
    private int populationSize;

    @Value("${algorithm.iterations.count:1000}")
    private int iterationsCount;

    @Value("${algorithm.probability.crossover:0.9}")
    private float crossoverProbability;

    @Value("${algorithm.probability.mutation:0.02}")
    private float mutationProbability;

    @Bean
    public GeneticAlgorithm geneticAlgorithm(InitializePopulation initializePopulation, Evaluation evaluation,
            Selection selection, Crossover crossover, Mutation mutation) {
        return new GeneticAlgorithm(iterationsCount, initializePopulation, evaluation, selection, crossover, mutation);
    }

    @Bean
    public InitializePopulation initializePopulation() {
        return new RandomInitialize(populationSize);
    }

    @Bean
    public Evaluation evaluation() {
        return new NoCollisionEvaluation();
    }

    @Bean
    public Selection selection() {
        return new TournamentSelection(populationSize);
    }

    @Bean
    public Crossover crossover() {
        return new OrderCrossover(crossoverProbability);
    }

    @Bean
    public Mutation mutation() {
        return new ExchangeMutation(mutationProbability);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
