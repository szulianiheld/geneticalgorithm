package com.geneticalgorithm.interfaces;

import java.util.List;

import com.geneticalgorithm.model.AlgorithmResult;

public interface InitializePopulation {

    List<AlgorithmResult> init(Integer number);

}
