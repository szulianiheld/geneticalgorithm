package com.geneticalgorithm.interfaces;

import java.util.List;

import com.geneticalgorithm.model.AlgorithmResult;

public interface Mutation {

    List<AlgorithmResult> mutate(List<AlgorithmResult> children);

}
