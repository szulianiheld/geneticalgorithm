package com.geneticalgorithm.interfaces;

import com.geneticalgorithm.model.AlgorithmResult;

import java.util.List;

public interface Selection {

    List<AlgorithmResult> select(List<AlgorithmResult> population);

}
