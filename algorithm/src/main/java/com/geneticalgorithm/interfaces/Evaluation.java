package com.geneticalgorithm.interfaces;

import com.geneticalgorithm.model.AlgorithmResult;

import java.util.List;

public interface Evaluation {

    AlgorithmResult eval(List<AlgorithmResult> population);

}
