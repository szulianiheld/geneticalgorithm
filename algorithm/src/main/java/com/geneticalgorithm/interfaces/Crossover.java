package com.geneticalgorithm.interfaces;

import com.geneticalgorithm.model.AlgorithmResult;

import java.util.List;

public interface Crossover {

    List<AlgorithmResult> cross(List<AlgorithmResult> parents);

}
