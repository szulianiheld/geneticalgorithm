package com.geneticalgorithm;

import com.geneticalgorithm.interfaces.Crossover;
import com.geneticalgorithm.interfaces.Evaluation;
import com.geneticalgorithm.interfaces.InitializePopulation;
import com.geneticalgorithm.interfaces.Mutation;
import com.geneticalgorithm.interfaces.Selection;
import com.geneticalgorithm.model.AlgorithmResult;
import com.geneticalgorithm.model.AlgorithmResultWrapper;

import java.util.ArrayList;
import java.util.List;

public class GeneticAlgorithm {

    private int iterationCount;
    private InitializePopulation initializePopulation;
    private Evaluation evaluation;
    private Selection selection;
    private Crossover crossover;
    private Mutation mutation;

    public GeneticAlgorithm(int iterationCount, InitializePopulation initializePopulation, Evaluation evaluation,
            Selection selection, Crossover crossover, Mutation mutation) {
        this.iterationCount = iterationCount;
        this.initializePopulation = initializePopulation;
        this.evaluation = evaluation;
        this.selection = selection;
        this.crossover = crossover;
        this.mutation = mutation;
    }

    public AlgorithmResultWrapper run(Integer number) {
        int iterations = 0;
        // The population is initialized. The population size is a fixed N.
        List<AlgorithmResult> population = initializePopulation.init(number);
        for (int count = 0; count < iterationCount; count++) {
            iterations += 1;
            // Check if some element from our population is a solution
            AlgorithmResult solution = evaluation.eval(population);
            if (solution != null) {
                return new AlgorithmResultWrapper(solution, iterations);
            }
            // Generate children from our population. This would be the possible new generation.
            List<AlgorithmResult> children = crossover.cross(population);
            // Apply some mutation to our new children
            List<AlgorithmResult> mutatedChildren = mutation.mutate(children);
            // The new children are joined to the new population in order to select the new generation
            List<AlgorithmResult> newPopulation = new ArrayList<>(population);
            newPopulation.addAll(mutatedChildren);
            // Select N amount of elements from our hole population, parents and children, to become the new generation used in the next iteration.
            population = selection.select(newPopulation);
        }
        return new AlgorithmResultWrapper(new AlgorithmResult(number), iterations);
    }

}
