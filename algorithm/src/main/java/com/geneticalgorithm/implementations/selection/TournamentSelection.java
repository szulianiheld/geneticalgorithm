package com.geneticalgorithm.implementations.selection;

import com.geneticalgorithm.interfaces.Selection;
import com.geneticalgorithm.model.AlgorithmResult;

import java.util.Comparator;
import java.util.List;

public class TournamentSelection implements Selection {

    private int populationSize;

    public TournamentSelection(int populationSize) {
        this.populationSize = populationSize;
    }

    @Override
    public List<AlgorithmResult> select(List<AlgorithmResult> population) {
        population.sort(Comparator.comparingInt(AlgorithmResult::collisions));
        return population.subList(0, populationSize);
    }

}
