package com.geneticalgorithm.implementations.crossover;

import com.geneticalgorithm.interfaces.Crossover;
import com.geneticalgorithm.model.AlgorithmResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class OrderCrossover implements Crossover {

    private float crossoverProbability;
    private Random random;

    public OrderCrossover(float crossoverProbability) {
        this.crossoverProbability = crossoverProbability;
        this.random = new Random();
    }

    @Override
    public List<AlgorithmResult> cross(List<AlgorithmResult> parents) {
        List<AlgorithmResult> children = new ArrayList<>();
        if (random.nextFloat() < crossoverProbability) {
            for (int i = 0, j = parents.size() - 1; i < j; i++, j--) {
                AlgorithmResult parent1 = parents.get(i);
                AlgorithmResult parent2 = parents.get(j);
                children.add(createChildren(parent1, parent2));
                children.add(createChildren(parent2, parent1));
            }
        }
        return children;
    }

    private AlgorithmResult createChildren(AlgorithmResult parent1, AlgorithmResult parent2) {
        int size = parent1.getSize();
        Integer[] child = new Integer[size];
        int minLimit = random.nextInt(size);
        int maxLimit = random.nextInt(size);
        if (maxLimit < minLimit) {
            int aux = minLimit;
            minLimit = maxLimit;
            maxLimit = aux;
        }
        List<Integer> inserted = new ArrayList<>();
        for (int i = minLimit; i <= maxLimit; i++) {
            child[i] = parent1.getValue(i);
            inserted.add(parent1.getValue(i));
        }
        int childIndex = nextIndex(size, maxLimit);
        for (int i = maxLimit + 1; i < size; i++) {
            if (!inserted.contains(parent2.getValue(i))) {
                child[childIndex] = parent2.getValue(i);
                inserted.add(parent2.getValue(i));
                childIndex = nextIndex(size, childIndex);
            }
        }
        for (int i = 0; i <= maxLimit; i++) {
            if (!inserted.contains(parent2.getValue(i))) {
                child[childIndex] = parent2.getValue(i);
                inserted.add(parent2.getValue(i));
                childIndex = nextIndex(size, childIndex);
            }
        }
        AlgorithmResult algorithmResult = new AlgorithmResult(size);
        algorithmResult.setElements(Arrays.asList(child));
        return algorithmResult;
    }

    private int nextIndex(int size, int actualIndex) {
        int nextIndex = actualIndex + 1;
        if (nextIndex < size) {
            return nextIndex;
        }
        return 0;
    }

}
