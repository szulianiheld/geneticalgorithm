package com.geneticalgorithm.implementations.evaluation;

import com.geneticalgorithm.interfaces.Evaluation;
import com.geneticalgorithm.model.AlgorithmResult;

import java.util.List;

public class NoCollisionEvaluation implements Evaluation {

    @Override
    public AlgorithmResult eval(List<AlgorithmResult> population) {
        for (AlgorithmResult result : population) {
            if (result.collisions() == 0) {
                return result;
            }
        }
        return null;
    }

}
