package com.geneticalgorithm.implementations.mutation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.geneticalgorithm.interfaces.Mutation;
import com.geneticalgorithm.model.AlgorithmResult;

public class ExchangeMutation implements Mutation {

    private float mutationProbability;
    private Random random;

    public ExchangeMutation(float mutationProbability) {
        this.mutationProbability = mutationProbability;
        this.random = new Random();
    }

    @Override
    public List<AlgorithmResult> mutate(List<AlgorithmResult> children) {
        List<AlgorithmResult> mutedChildren = new ArrayList<>();
        if (random.nextFloat() < mutationProbability) {
            for (AlgorithmResult child : children) {
                mutedChildren.add(mutateChild(child));
            }
        } else {
            return children;
        }
        return mutedChildren;
    }

    private AlgorithmResult mutateChild(AlgorithmResult child) {
        int size = child.getSize();
        int index1 = random.nextInt(size);
        int index2 = random.nextInt(size);
        Collections.swap(child.getElements(), index1, index2);
        return child;
    }

}
