package com.geneticalgorithm.implementations.initialize;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.geneticalgorithm.interfaces.InitializePopulation;
import com.geneticalgorithm.model.AlgorithmResult;

public class RandomInitialize implements InitializePopulation {

    private int populationSize;

    public RandomInitialize(int populationSize) {
        this.populationSize = populationSize;
    }

    public List<AlgorithmResult> init(Integer number) {
        List<AlgorithmResult> firstPopulation = new ArrayList<>();
        for (int i = 0; i < populationSize; i++) {
            firstPopulation.add(createRandomResult(number));
        }
        return firstPopulation;
    }

    private AlgorithmResult createRandomResult(Integer number) {
        List<Integer> elements = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            elements.add(i);
        }
        AlgorithmResult randomSolution = new AlgorithmResult(number);
        Collections.shuffle(elements);
        randomSolution.setElements(elements);
        return randomSolution;
    }

}
