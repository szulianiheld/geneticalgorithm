package com.geneticalgorithm.controller;

import com.geneticalgorithm.GeneticAlgorithm;
import com.geneticalgorithm.model.AlgorithmResultWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/geneticalgorithm"})
public class GeneticAlgorithmController {

    @Autowired
    private GeneticAlgorithm geneticAlgorithm;

    @GetMapping(value = "{number}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> getRemoteItem(@PathVariable("number") final Integer number) {
        AlgorithmResultWrapper wrapper = geneticAlgorithm.run(number);
        if (wrapper.getAlgorithmResult().isEmpty()) {
            return ResponseEntity.ok("Solution not found in " + wrapper.getIterations() + " iterations.");
        }
        return ResponseEntity.ok(wrapper.toString());
    }

}
